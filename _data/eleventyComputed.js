const fetch = require("node-fetch");

module.exports = {
    stars: async (data) => {
        return Promise.all(data.projects.projects.map(async (project) => {
            let res = await fetch(project.api_url);
            let json = await res.json();
            return [project.name, json.star_count];
        })).then(res => {
            if (res.filter(r => r === undefined).length) {
                return undefined;
            } else {
                return new Map(res.map(r => [r[0], r[1] ?? 0]));
            }
        }).catch(() => {
            return new Map(data.projects.projects.map((project) => [project, 0]));
        });
    }
};