module.exports.data = {
    permalink: "feed.json",
    eleventyExcludeFromCollections: true,
    normalizeAbsoluteUrls: true,
    metadata: {
        title: "OpenCode @ MIT Blog",
        subtitle: "Open Source from the MIT community.",
        language: "en",
        url: "https://opencode-mit.gitlab.io/",
    }
};

module.exports.render = async function ({ members, permalink, metadata, collections, normalizeAbsoluteUrls }) {
    let feed = {
        version: "https://jsonfeed.org/version/1.1",
        title: metadata.title,
        language: metadata.language,
        home_page_url: metadata.url,
        feed_url: this.absoluteUrl(this.url(permalink), metadata.url),
        description: metadata.subtitle,
        items: []
    };

    for (let post of collections.posts.slice().reverse()) {
        let absolutePostUrl = this.absoluteUrl(this.url(post.url), metadata.url);
        let author = members.members.find(m => m.username == post.data.author);
        
        let relevant_info = {
            name: author.name,
            avatar:  this.absoluteUrl(this.url(author.image), metadata.url),
        };

        if (author.outside_links !== undefined && author.outside_links.length) {
            relevant_info.url = author.outside_links[0];
        }

        let item = {
            id: absolutePostUrl,
            url: absolutePostUrl,
            title: post.data.title,
            summary: post.data.subtext,
            image: this.absoluteUrl(post.data.thumbnail, metadata.url),
            banner_image: this.absoluteUrl(post.data.thumbnail, metadata.url),
            authors: [relevant_info],
            date_published: this.dateToRfc3339(post.date),
            content_html: post.templateContent,
        };
        if (normalizeAbsoluteUrls) {
            item.content_html = await this.htmlToAbsoluteUrls(item.content_html, absolutePostUrl);
        }
        feed.items.push(item);
    }

    return JSON.stringify(feed, null, 2);
};